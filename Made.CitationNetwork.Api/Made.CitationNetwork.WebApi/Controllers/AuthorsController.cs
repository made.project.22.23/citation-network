﻿using Made.CitationNetwork.BLL.Services.Interfaces;
using Made.CitationNetwork.DAL.Interfaces;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Made.CitationNetwork.WebApi.Controllers
{
    [Route("api/authors")]
    [ApiController]
    public class AuthorsController : ControllerBase
    {
        private readonly IAuthorsService authorsService;

        public AuthorsController(IAuthorsService authorsService)
        {
            this.authorsService = authorsService ?? throw new ArgumentNullException(nameof(authorsService));
        }

        [HttpGet]
        [Route("all")]
        public async Task<IActionResult> GetAllAuthors()
        {
            var authors = await authorsService.GetAllAuthorsAsync();

            return Ok(authors);
        }
    }
}
