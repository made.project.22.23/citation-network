﻿using Made.CitationNetwork.BLL.Services.Interfaces;
using Made.CitationNetwork.DAL.Interfaces;
using Made.CitationNetwork.DAL.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Made.CitationNetwork.BLL.Services
{
    public class AuthorsService : BaseService, IAuthorsService
    {
        public AuthorsService(IUnitOfWork unitOfWork) : base(unitOfWork)
        {
        }

        public async Task<IEnumerable<Authors>> GetAllAuthorsAsync()
        {
            var authors = await UnitOfWork.Authors
                .GetAll()
                .Include(a => a.ArticlesAuthors).ThenInclude(aa => aa.ArticleNavigation)
                .Include(a => a.Orgs)
                .ToListAsync();

            return authors;
        }
    }
}
