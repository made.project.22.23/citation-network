﻿using Made.CitationNetwork.BLL.Services;
using Made.CitationNetwork.BLL.Services.Interfaces;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Text;

namespace Made.CitationNetwork.BLL.DI
{
    public static class DependencyRegistrator
    {
        public static IServiceCollection RegisterServices(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddScoped<IArticlesService, ArticlesService>();
            services.AddScoped<IAuthorsService, AuthorsService>();

            return services;
        }
    }
}
