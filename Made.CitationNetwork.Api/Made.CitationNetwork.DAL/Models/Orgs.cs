﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Made.CitationNetwork.DAL.Models
{
    public class Orgs
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string AuthorId { get; set; }

        public virtual Authors Author { get; set; }
    }
}
