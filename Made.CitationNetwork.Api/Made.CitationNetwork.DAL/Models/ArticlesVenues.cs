﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Made.CitationNetwork.DAL.Models
{
    public class ArticlesVenues
    {
        public string ArticleId { get; set; }
        public string VenueId { get; set; }

        public virtual Articles ArticleNavigation { get; set; }
        public virtual Venues VenueNavigation { get; set; }
    }
}
