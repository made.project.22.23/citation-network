﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Made.CitationNetwork.DAL.Models
{
    public class Venues
    {
        public string Id { get; set; }
        public string Name { get; set; }

        public ICollection<ArticlesVenues> ArticlesVenues { get; set; }
    }
}
