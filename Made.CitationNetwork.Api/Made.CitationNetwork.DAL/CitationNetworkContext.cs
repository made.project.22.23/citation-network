﻿using System;
using Made.CitationNetwork.DAL.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

// Code scaffolded by EF Core assumes nullable reference types (NRTs) are not used or disabled.
// If you have enabled NRTs for your project, then un-comment the following line:
// #nullable disable

namespace Made.CitationNetwork.DAL
{
    public partial class CitationNetworkContext : DbContext
    {
        public CitationNetworkContext()
        {
        }

        public CitationNetworkContext(DbContextOptions<CitationNetworkContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Articles> Articles { get; set; }
        public virtual DbSet<Authors> Authors { get; set; }
        public virtual DbSet<Orgs> Orgs { get; set; }
        public virtual DbSet<Venues> Venues { get; set; }
        public virtual DbSet<Abstract> Abstract { get; set; }
        public virtual DbSet<Fos> Fos { get; set; }
        public virtual DbSet<Keywords> Keywords { get; set; }
        public virtual DbSet<References> References { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Abstract>(entity =>
            {
                entity.ToTable("Abstract");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Text).HasColumnName("abstract");

                entity.Property(e => e.ArticleId)
                    .HasColumnName("article_id")
                    .HasMaxLength(100);

                entity.HasOne(d => d.Article)
                    .WithMany(p => p.Abstract)
                    .HasForeignKey(d => d.ArticleId);
            });

            modelBuilder.Entity<Articles>(entity =>
            {
                entity.ToTable("Articles");

                entity.HasKey(e => e.Id);

                entity.Property(e => e.Id)
                    .HasColumnName("id_article")
                    .HasMaxLength(100);

                entity.Property(e => e.GlobalCitationNumber).HasColumnName("n_citation_global");

                entity.Property(e => e.LocalCitationNumber).HasColumnName("n_citation_local");

                entity.Property(e => e.Title).HasColumnName("title");

                entity.Property(e => e.Year).HasColumnName("year");
            });

            modelBuilder.Entity<ArticlesAuthors>(entity =>
            {
                entity.ToTable("ArticlesAuthors");

                entity.HasKey(aa => new { aa.ArticleId, aa.AuthorId });

                entity.Property(e => e.ArticleId)
                    .HasColumnName("id_article")
                    .HasMaxLength(100);

                entity.Property(e => e.AuthorId)
                    .HasColumnName("id_author")
                    .HasMaxLength(100);

                entity.HasOne(d => d.ArticleNavigation)
                    .WithMany(a => a.ArticlesAuthors)
                    .HasForeignKey(d => d.ArticleId);

                entity.HasOne(d => d.AuthorNavigation)
                    .WithMany(a => a.ArticlesAuthors)
                    .HasForeignKey(d => d.AuthorId);
            });

            modelBuilder.Entity<Authors>(entity =>
            {
                entity.ToTable("Authors");

                entity.HasKey(e => e.Id);

                entity.Property(e => e.Id)
                    .HasColumnName("id_author")
                    .HasMaxLength(100);

                entity.Property(e => e.Name)
                    .HasColumnName("name")
                    .HasMaxLength(1000);
            });

            modelBuilder.Entity<ArticlesVenues>(entity =>
            {
                entity.ToTable("ArticlesVenues");

                entity.HasKey(av => new { av.ArticleId, av.VenueId });

                entity.Property(e => e.ArticleId)
                    .HasColumnName("id_article")
                    .HasMaxLength(100);

                entity.Property(e => e.VenueId)
                    .HasColumnName("id_venue")
                    .HasMaxLength(100);

                entity.HasOne(d => d.ArticleNavigation)
                    .WithMany(a => a.ArticlesVenues)
                    .HasForeignKey(d => d.ArticleId);

                entity.HasOne(d => d.VenueNavigation)
                    .WithMany(a => a.ArticlesVenues)
                    .HasForeignKey(d => d.VenueId);
            });

            modelBuilder.Entity<Venues>(entity =>
            {
                entity.ToTable("Venues");

                entity.HasKey(e => e.Id);

                entity.Property(e => e.Id)
                    .HasColumnName("id_venue")
                    .HasMaxLength(100);

                entity.Property(e => e.Name)
                    .HasColumnName("name");
            });

            modelBuilder.Entity<Orgs>(entity =>
            {
                entity.ToTable("Orgs");

                entity.HasKey(e => e.Id);

                entity.Property(e => e.Id)
                    .HasColumnName("id");

                entity.Property(e => e.Name)
                    .HasColumnName("org")
                    .HasMaxLength(1000);

                entity.Property(e => e.AuthorId)
                    .HasColumnName("id_author")
                    .HasMaxLength(100);

                entity.HasOne(d => d.Author)
                    .WithMany(p => p.Orgs)
                    .HasForeignKey(d => d.AuthorId);
            });

            modelBuilder.Entity<Fos>(entity =>
            {
                entity.ToTable("Fos");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.ArticleId)
                    .HasColumnName("article_id")
                    .HasMaxLength(100);

                entity.Property(e => e.FosText)
                    .HasColumnName("fos")
                    .HasMaxLength(1000);

                entity.HasOne(d => d.Article)
                    .WithMany(p => p.Fos)
                    .HasForeignKey(d => d.ArticleId);
            });

            modelBuilder.Entity<Keywords>(entity =>
            {
                entity.ToTable("Keywords");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.ArticleId)
                    .HasColumnName("article_id")
                    .HasMaxLength(100);

                entity.Property(e => e.Sentence)
                    .HasColumnName("keyword")
                    .HasMaxLength(1000);

                entity.HasOne(d => d.Article)
                    .WithMany(p => p.Keywords)
                    .HasForeignKey(d => d.ArticleId);
            });

            modelBuilder.Entity<References>(entity =>
            {
                entity.ToTable("References");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.ArticleId)
                    .HasColumnName("article_id")
                    .HasMaxLength(100);

                entity.Property(e => e.Ref)
                    .HasColumnName("reference")
                    .HasMaxLength(1000);

                entity.HasOne(d => d.Article)
                    .WithMany(p => p.References)
                    .HasForeignKey(d => d.ArticleId);
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
