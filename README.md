# citation-network-api

## Prerequisities

This sample **requires** prerequisites in order to run.

### Install Docker & docker-compose

- [Windows instructions](https://docs.docker.com/desktop/install/windows-install/)
- [Linux instructions](https://docs.docker.com/desktop/install/linux-install/)

## To run locally:

- Navigate to the root folder
- Run `docker-compose up -d`
- Create db using `./scripts/init-db.sql`

## Swagger api

- Navigate to http://localhost:8001/api/citation-network/docs/index.html